%SAVESETUP saves a mrx setup to a setup file.
%
%   SAVESETUP(filename,setup) saves setup in filename. In case a file
%   already exist it prompts for furher instructions. SAVESETUP
%   automatically adds .mrxsetup extension to filename.
%
%   SAVESETUP(filename,setup,varargin) allows optional setting/parameter:
%   	'override'      overrides filename in case a file already exist
%                       without promting.
%   	'ff'            fast forward - skips any promts


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function saveSetup(filename,setup,varargin)

%% parse inputs
p = inputParser;
p.addRequired('filename');
p.addRequired('setup');
p.addParameter('override', false, @islogical);
p.addParameter('ff', false, @islogical);

p.parse(filename,setup,varargin{:});

filename        = p.Results.filename;
override        = p.Results.override;
ff              = p.Results.ff;

dim             = p.Results.setup.dim;
roi             = p.Results.setup.roi;
coils           = p.Results.setup.coils;
sensors         = p.Results.setup.sensors;
if isfield(p.Results.setup, 'info')
    info        = p.Results.setup.info;
else
    info        = [];
end
if ~isfield(info, 'name')
    prompt = 'The setup name was not specified (setup.info.name is not set). Please type a name: ';
    
    info.name = [];
    while isempty(info.name)
        info.name = input(prompt,'s');
        if isempty(info.name)
            disp('the name may not be empty!')
        end
        if strcmpi(info.name, 'a name')
            disp('very funny, we try that again!');
            info.name = [];
        end
    end
end
if ~isfield(info, 'variant')
    prompt = 'The setup variant was not specified (setup.info.variant is not set). Please type a variant name: ';
    
    info.variant = [];
    while isempty(info.variant)
        info.variant = input(prompt,'s');
        if isempty(info.variant)
            disp('the variant name may not be empty!')
        end
        if strcmpi(info.variant, 'a variant name')
            disp('oh come on please!');
            info.variant = [];
        end
    end
end
if isfield(p.Results.setup, 'coilGroups')
    coilGroups  = p.Results.setup.coilGroups;
else
    coilGroups  = {1:numel(coils)};
end
if isfield(p.Results.setup, 'sensorGroups')
    sensorGroups = p.Results.setup.sensorGroups;
else
    sensorGroups = {1:numel(sensors)};
end

skip = false;

%% check if setup is valid
if ~isSetupValid(setup)
    prompt = 'The setup is not valid! Save nevertheless (NOT RECOMMENDED)? Y/N [N]: ';
    if ff
        str = 'N';
        disp([prompt, str]);
    else
        str = input(prompt,'s');
        if isempty(str)
            str = 'N';
        end
    end
    if ~any(strcmp(str,{'Y','y','yes'}))
        skip=true;
        fpop('setup creation:','skipped!');
    end
end


%% add .mrxsetup if missing
if length(filename) < 9 || ~strcmp(filename(end-8:end),'.mrxsetup')
    filename = [filename,'.mrxsetup'];
end

%% create folder if not existent
filepath = fileparts(filename);
if ~exist(filepath,'dir')&&~isempty(filepath)
    prompt = ['folder ', filepath, ' does not exist! Create? Y/N [Y]: '];
    
    if ff
        str = 'Y';
        disp([prompt, str]);
    else
        str = input(prompt,'s');
        if isempty(str)
            str = 'Y';
        end
    end
    
    if any(strcmp(str,{'Y','y','yes'}))
        mkdir(filepath);
        fpop('folder creation:','done!');
    else
        skip=true;
        fpop('setup creation:','skipped!');
    end
end


%% function
if exist(filename, 'file')&&~override
    prompt = [filename, ' already exist! Override? Y/N [N]: '];
    
    if ff
        str = 'N';
        disp([prompt, str]);
    else
        str = input(prompt,'s');
        if isempty(str)
            str = 'N';
        end
    end
    
    if ~any(strcmp(str,{'Y','y','yes'}))
        skip=true;
        fpop('setup creation:','skipped!');
    end
end

if ~skip
    if isOctave()
        save(filename, '-mat7-binary', 'dim', 'roi', 'coils', 'sensors', 'info', 'coilGroups', 'sensorGroups');
    else
        save(filename, '-mat', 'dim', 'roi', 'coils', 'sensors', 'info', 'coilGroups', 'sensorGroups');
    end
    fpop('setup creation:','done!');
end



end

