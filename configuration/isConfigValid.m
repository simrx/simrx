% ISCONFIGVALID checks if a given config is valid.
%
%   integrity = ISCONFIGVALID(config) checks if the provided config is
%   valid. If all checks are passed successfully the function return true,
%   otherwise returns false.
%
%   The following checks are performed:
%       * coilsActive is set
%       * sensorsActive is set
%       * currentPattern is set
%       * measurementPattern is set
%       * info is set
%       * info.name is set
%       * info.variant is set
%       * currentPattern is compatible to coilsActive
%       * currentPattern does not contain any zeros rows
%       * measurementPattern is compatible to sensorsActive
%       * measurementPattern does not contain any zeros rows
%       * currentPattern is compatible to measurementPattern


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function integrity = isConfigValid(config)

%% assume something goes wrong ;)
integrity = false;


%% check if all config variables are set
if ~isfield(config, 'coilsActive'); warning('config.coilsActive not set!'); return; end
if ~isfield(config, 'sensorsActive'); warning('config.sensorsActive not set!'); return; end
if ~isfield(config, 'currentPattern'); warning('config.currentPattern not set!'); return; end
if ~isfield(config, 'measurementPattern'); warning('config.measurementPattern not set!'); return; end
if ~isfield(config, 'info'); warning('config.info not set!'); return; end
if ~isfield(config.info, 'name'); warning('config.info.name not set!'); return; end
if ~isfield(config.info, 'variant'); warning('config.info.variant not set!'); return; end


%% check if the current pattern fits the active coils
if ~(size(config.currentPattern, 2)==numel(config.coilsActive)); warning('config.currentPattern and config.coilsActive are not compatible!'); return; end


%% check if config contains empty current patterns
if ~(all(any(config.currentPattern'))); warning('config.currentPattern contains empty current patterns!'); return; end


%% check if the measurement pattern fits the active sensors
if ~(size(config.measurementPattern, 2)==numel(config.sensorsActive)); warning('config.measurementPattern and config.sensorsActive are not compatible!'); return; end


%% check if config contains empty current patterns
if ~(all(any(config.measurementPattern'))); warning('config.measurementPattern contains empty patterns!'); return; end


%% check if measurement pattern fits number of current patterns
if ~(size(config.currentPattern, 1) == size(config.measurementPattern, 1)); warning('number of patterns in config.measurementPattern and config.currentPattern do not fit!'); return; end


%% template for a new rule
if ~(true); warning('something went wrong!'); return; end


%% return true if everything passed
integrity = true;


end

