%CREATECOILLOOP creates a 3D point cloud that describes a coil.
%
%   out = createCoilLoop(rad,w,nSeg) creates a point cloud that
%   defines a coil. The arguments are:
%       'radius'        the radius of the coil,
%       'windings'      the winding number,
%       'numOfSegments' the number of segments used.
%
%   out = createCoilLoop(...,type) creates a coil with a certain shape:
%       'type'          coil shape: available options: 'loop' (default) and
%                           'spriral'.
%
%   run
%       CREATECOILLOOP
%   without arguments to see a minimal example.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function out = createCoilLoop(radius, windings, numOfSegments, type)

%% run example?
if nargin == 0
    out = runExample;
    return;
end

%function code starts here
if nargin < 4
    type = 'loop';
end

out = zeros([numOfSegments+1,3]);
th  = 0:2*windings*pi/numOfSegments:2*windings*pi;

switch type
    case 'spiral'
        out(:,1)    = radius*th.*cos(th)/(2*pi*windings);
        out(:,2)    = radius*th.*sin(th)/(2*pi*windings);
    otherwise %loop
        out(:,1)    = radius.*cos(th');
        out(:,2)    = radius.*sin(th');
end


%% example
    function out = runExample()
        
        radius       	= 1;
        windings       	= 3;
        numOfSegments   = 19;
        type            = 'spiral';
        
        out = createCoilLoop(radius, windings, numOfSegments, type);
        
        plot3(out(:,1),out(:,2),out(:,3));
        view(0,90)
        axis equal;
        hold off;
        
        
    end


end

