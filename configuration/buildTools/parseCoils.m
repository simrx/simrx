%PARSECOILS uses a template point cloud and place in multiple places
%
%   out = PARSECOILS(coils,loop) takes a list coils (with coil postions and
%   normals) and places the template loop of a coil at each position.
%   Internally it uses RELOCATESTRUCTURE.
%
%   just run:
%       PARSECOILS
%
%   to see a minimal example.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function coils = parseCoils(coils, loop)

%% run example?
if nargin == 0
    coils = runExample;
    return;
end


%% function
if isempty(loop)
    if isfield(coils, 'Segments')
        coils = rmfield(coils, 'Segments');
    end
else
    for i = 1:size(coils,2)
        coils(i).Segments       = relocateStructure(loop,[0,0,1],coils(i).Normal,coils(i).Position);
    end
end

%% example
    function coils = runExample()
        
        coils = createEntityArray;
        loop = createCoilLoop;
        
        coils = parseCoils(coils,loop);
        
    end


end

