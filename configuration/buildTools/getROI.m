%GETROI returns the smallest cartasian intervals that contain the point
%   cloud defined by a given voxel grid. Note that it only support
%   cartesian coordinates with uniformly distributed point cloud.
%
%   roi = getROI(voxelGrid) returns a struct roi with boundaries as fields
%   x,y,z (namely roi.x, roi.y, roi.z)
%
%   [roiX, roiY, roiZ] = getROI(voxelGrid) returns arrays roiX, roiY, roiZ
%   which hold the boundary information
%
%   [...] = getROI(..., roundTol) allows to set the exponent of the
%   rounding tolerance by hand (default: 5, which translates to 10^-5).


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function [roi, roiY, roiZ] = getROI(voxelGrid)

%function code starts here
if nargin < 2
    roundTol = 5;
end


x = unique(voxelGrid(:,1));
y = unique(voxelGrid(:,2));
z = unique(voxelGrid(:,3));

if isOctave() % octaves round function does not support a precision
    stepX = round(mean(diff(x))*(10^roundTol))/(10^roundTol);
    stepY = round(mean(diff(y))*(10^roundTol))/(10^roundTol);
    stepZ = round(mean(diff(z))*(10^roundTol))/(10^roundTol);
else
    stepX = round(mean(diff(x)),roundTol);
    stepY = round(mean(diff(y)),roundTol);
    stepZ = round(mean(diff(z)),roundTol);
end

if isnan(stepX)
    stepX = 0;
end
if isnan(stepY)
    stepY = 0;
end
if isnan(stepZ)
    stepZ = 0;
end

minX = zeros([1,numel(x)]);
maxX = zeros([1,numel(x)]);
minY = zeros([1,numel(y)]);
maxY = zeros([1,numel(y)]);
minZ = zeros([1,numel(z)]);
maxZ = zeros([1,numel(z)]);

for i = 1:numel(x)
    minX(i) = x(i)-(stepX/2)-((i-1)*stepX);
    maxX(i) = x(i)+(stepX/2)+((numel(x)-i)*stepX);
end
for i = 1:numel(y)
    minY(i) = y(i)-(stepY/2)-((i-1)*stepY);
    maxY(i) = y(i)+(stepY/2)+((numel(y)-i)*stepY);
end
for i = 1:numel(z)
    minZ(i) = z(i)-(stepZ/2)-((i-1)*stepZ);
    maxZ(i) = z(i)+(stepZ/2)+((numel(z)-i)*stepZ);
end

if nargout == 1
    roi.x   = [mean(minX),mean(maxX)];
    roi.y   = [mean(minY),mean(maxY)];
    roi.z   = [mean(minZ),mean(maxZ)];
else
    roi     = [mean(minX),mean(maxX)];
    roiY    = [mean(minY),mean(maxY)];
    roiZ    = [mean(minZ),mean(maxZ)];
end


end

