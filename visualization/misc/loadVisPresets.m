%LOADVISPRESETS contains presets for visualization tools


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021

function presets = loadVisPresets()

presets.colors.solBase03    = [  0,  43,  54]./255;
presets.colors.solBase02    = [  7,  54,  66]./255;
presets.colors.solBase01    = [ 88, 110, 117]./255;
presets.colors.solBase00    = [101, 123, 131]./255;
presets.colors.solBase0     = [131, 148, 150]./255;
presets.colors.solBase1     = [147, 161, 161]./255;
presets.colors.solBase2     = [238, 232, 213]./255;
presets.colors.solBase3     = [253, 246, 227]./255;
presets.colors.solYellow    = [181, 137,   0]./255;
presets.colors.solOrange    = [203,  75,  22]./255;
presets.colors.solRed       = [220,  50,  47]./255;
presets.colors.solMagenta   = [211,  54, 130]./255;
presets.colors.solViolet    = [108, 113, 196]./255;
presets.colors.solBlue      = [ 38, 139, 210]./255;
presets.colors.solCyan      = [ 42, 161, 152]./255;
presets.colors.solGreen     = [133, 153,   0]./255;

presets.coilColor           = presets.colors.solOrange;
presets.coilColorActive     = presets.colors.solYellow;
presets.sensorColor         = presets.colors.solCyan;
presets.fieldColor          = presets.colors.solViolet;
presets.roiColor            = presets.colors.solBase1;


presets.coilArrowScale                  = .01;
presets.coilActiveArrowScale            = .015;
presets.fieldArrowScale                 = .003;
presets.sensorArrowScale                = .01;

presets.axisRegionDrawProperties        = {'FaceColor', 'none', 'EdgeAlpha', 0.005};
presets.roiDrawProperties               = {'FaceColor', presets.roiColor, 'FaceAlpha', 0.3'};
presets.sensorDrawProperties            = {'Color', presets.sensorColor, 'LineWidth', 1.5, 'MaxHeadSize', 2};
presets.coilDrawProperties              = {'Color', presets.coilColor, 'LineWidth', 2.0};
presets.coilDrawPropertiesLine          = {};
presets.coilDrawPropertiesArrow         = {'MaxHeadSize', 2};
presets.coilLiveDrawProperties          = {'Color', presets.coilColorActive, 'LineWidth', 2.5};
presets.coilLiveDrawPropertiesLine      = {};
presets.coilLiveDrawPropertiesArrow     = {'MaxHeadSize', 2};
presets.fieldDrawProperties             = {'Color', presets.fieldColor};

end