%DRAWROI visualizes the region of interest
%
%   h = DRAWROI(roi) visualizes the the region of interst. The returned h
%   is the respective function handle.
%
%   h = DRAWROI(..., varargin) allows optional
%   settings/parameter:
%       'roiDrawProperties' Cell of Line Properties that is forwarded to the
%                       respective calls
%       'shift'         shifts the object by given vector
%                       (default: [0,0,0])


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function h = drawROI(roi, varargin)

presets = loadVisPresets();

%% parse inputs
p = inputParser;
p.addParameter('roiDrawProperties', presets.roiDrawProperties);
p.addParameter('shift', [0,0,0], @(s) (isnumeric(s) && numel(s)==3));

p.parse(varargin{:});

lineProperties 	= p.Results.roiDrawProperties;
shift           = p.Results.shift;


%% function
if range(roi.z) == 0 % check if 2D or 3D
    omega = [roi.x+shift(1);roi.y+shift(2)];
else
    omega = [roi.x+shift(1);roi.y+shift(2);roi.z+shift(3)];
end

roiCorners	= omega(:,1)';
roiSize     = diff(omega,1,2)';

if range(roi.z) == 0 % check if 2D or 3D
    face    = [1 2 3 4];
    vertex  = [1 1; 0 1; 0 0; 1 0];
    vertices= [roiCorners(1)+vertex(:,1)*roiSize(1), roiCorners(2)+vertex(:,2)*roiSize(2)];
else
    face	= [1 2 3 4; 4 3 5 6; 6 7 8 5; 1 2 8 7; 6 7 1 4; 2 3 5 8];
    vertex	= [1 1 0; 0 1 0; 0 1 1; 1 1 1; 0 0 1; 1 0 1; 1 0 0; 0 0 0];
    vertices= [roiCorners(1)+vertex(:,1)*roiSize(1), roiCorners(2)+vertex(:,2)*roiSize(2), roiCorners(3)+vertex(:,3)*roiSize(3)];
end

h = patch('Faces',face,'Vertices',vertices, lineProperties{:});


end

