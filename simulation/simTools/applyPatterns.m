%APPLYPATTERNS applies a coil activation scheme to a simulated raw system.
%
%   A = APPLYPATTERNS(ARaw, config) processes a set of coil activations
%   specified by config.currentPattern for a given simulated system ARaw
%   into a matrix A. APPLYPATTERNS can handle 2D and 3D setups.
%
%   A = APPLYPATTERNS(..., varargin) allows optional
%   settings/parameter:
%       'coilList'      subset of config.coilsActive that are used for the
%                       derivation of the system
%       'sensorList'    subset of config.sensorsActive that are used for
%                       the derivation of the system


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function A = applyPatterns(ARaw, config, varargin)

%% parse inputs
p = inputParser;
p.addRequired('ARaw');
p.addRequired('config');
p.addParameter('coilList',config.coilsActive);
p.addParameter('sensorList',config.sensorsActive);
p.parse(ARaw, config, varargin{:});

ARaw        = p.Results.ARaw;
config      = p.Results.config;

coilList    = 1:numel(config.coilsActive);
coilInd     = ismember(config.coilsActive,p.Results.coilList);

% as of Nov 2019, the function unique does not accept parameter 'stable',
% which return an UNSORTED list of unique elements.
if isOctave
    [~,i,~]     = unique(coilList(coilInd), 'first');
    coilList    = coilList(sort(i));
else
    coilList    = unique(coilList(coilInd), 'stable');
end

numOfCoils  = numel(coilList);

if isempty(coilList)
    error('no coils selected with current settings');
end

sensorList      = 1:numel(config.sensorsActive);
sensorInd       = ismember(config.sensorsActive,p.Results.sensorList);

% as of Nov 2019, the function unique does not accept parameter 'stable',
% which return an UNSORTED list of unique elements.
if isOctave
    [~,i,~]     = unique(sensorList(sensorInd), 'first');
    sensorList  = sensorList(sort(i));
else
    sensorList  = unique(sensorList(sensorInd), 'stable');
end

if isempty(sensorList)
    error('no sensors selected with current settings');
end


%% get number of unknown
n = size(ARaw{1},2);


%% remove empty patterns

% indicator for current pattern rows that contain at least one
% activation
currentPatternInd           = any(config.currentPattern(:,coilList)');

% indicator for measurement pattern rows that contain at least one
% measurement
measurementPatternInd       = any(config.measurementPattern(:,sensorList)');

% inducator of patterns that have at least one activation AND
% measurement
patternInd = and(currentPatternInd, measurementPatternInd);

if ~any(patternInd)
    error('there are no patterns left that contain action');
end

% remove empty patterns
remainingCurrentPattern       = config.currentPattern(patternInd,coilList);
remainingMeasurementPattern   = logical(config.measurementPattern(patternInd,sensorList));

numOfPatterns               = nnz(patternInd);


%%
A = zeros([nnz(remainingMeasurementPattern),n]);
AOffset = 0;                                                                % define offset, since not every pattern has the same number of measurements

for i = 1:numOfPatterns
    currentCurrentPattern = remainingCurrentPattern(i, :);                  % set current current pattern
    currentMeasurementPattern = remainingMeasurementPattern(i, :);          % set current measurement pattern
    curNumOfSensors = nnz(currentMeasurementPattern);                       % get active number of sensors for current pattern
    ATemp = zeros([curNumOfSensors,n]);                                     % define size of this patterns system matrix
    for j = 1:numOfCoils                                                    % loop through all coils to check for activations
        curCoil = coilList(j);                                              % set current coil
        if currentCurrentPattern(j) ~= 0                                    % check if current coil is active
            ATemp = ATemp + ARaw{curCoil}(sensorList(currentMeasurementPattern),:) .* currentCurrentPattern(j); % add respective ARaw to this activations system matrix
        end
    end
    A(AOffset+1:AOffset+curNumOfSensors,:) = ATemp;                         % implant complete system matrix for this pattern in full matrix
    AOffset = AOffset + curNumOfSensors;                                    % update offset
end


end

