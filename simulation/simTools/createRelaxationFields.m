%CREATERELAXATIONFIELDS derives magnetic fields in sensors, that are
%induced by the aligned particles on the voxelgrid

%CREATERELAXATIONFIELDS calculates magnetic fields induced by dipoles.
%
%   ARaw = CREATEEXCITATIONFIELDS(sensors, fields, voxelGrid) derives
%   the magnetic field induced by dipoles in voxelGrid. The magnetization
%   of the dipoles was induced by a magnetic field provided by magnFields.
%
%   CREATEEXCITATIONFIELDS(..., verbose) allows to control the output of a
%   progress bar.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       22-May-2021


function ARaw = createRelaxationFields(sensors, magnFields, voxelGrid, verbose)

if nargin<4
    verbose = true;
end
numOfSensors    = size(sensors, 2);
numOfCoils      = numel(magnFields);

ARaw            = cell(numOfCoils,1);

if verbose
    fpo('Derive Dipole Fields: ');
    numOfPrintFeedback = 20;
    printInterval = ceil(linspace(1,numOfSensors,numOfPrintFeedback+1));
    printInterval = unique(printInterval(2:end));
end

for i = 1:numOfSensors
    dipoleField = createDipoleField(sensors(i), voxelGrid);
    for k = 1:numOfCoils
        ARaw{k}(i,:) = dot(dipoleField', magnFields{k}');
    end
    if verbose && any(i==printInterval)
        fprintf('#');
    end
end

if verbose
    for i = 1:(numOfPrintFeedback-numel(printInterval))
        fprintf('#');
    end
    fprintf('\n');
end


end

