%CREATEMRXMATRIXBYCOIL derives a MRX system matrix
%
%   ARaw = CREATEMRXMATRIXBYCOIL(setup, config, res) simulates the MRX
%   system specified in setup and config. The simulation resolution is
%   given by res. The given setup is simulated for each coil subsequently.
%   CREATEMRXMATRIXBYCOIL can handle 2D and 3D setups.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       22-May-2021


function A = createMRXMatrixByCoil(setup, config, res)

%% check setup and config compatibility
if ~checkCompatibility(setup, config); error('setup and config are not compatible'); end


%% create voxelgrid
voxelGrid = createVoxelGrid(setup.roi, res);


%% calculate magnetic fields induced by coils
magnFields = createExcitationFields(setup.coils(config.coilsActive), voxelGrid);


%% calculate dipole induced fields
ARaw = createRelaxationFields(setup.sensors(config.sensorsActive), magnFields, voxelGrid);


%% apply current and measurement pattern
A       = applyPatterns(ARaw, config);


end

