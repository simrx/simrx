%CREATEMRXMATRIXBYPATTERN derives a MRX system matrix
%
%   ARaw = CREATEMRXMATRIXBYPATTERN(setup, config, res) simulates the MRX
%   system specified in setup and config. The simulation resolution is
%   given by res. The given setup is simulated for each pattern
%   subsequently. CREATEMRXMATRIXBYCOIL can handle 2D and 3D setups.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       22-May-2021


function A = createMRXMatrixByPattern(setup, config, res, verbose)

%% check setup and config compatibility
if ~checkCompatibility(setup, config); error('setup and config are not compatible'); end


%% derive system matrix for each pattern

if nargin < 4
    verbose = true;
end

voxelGrid       = createVoxelGrid(setup.roi, res);
numOfPatterns   = size(config.currentPattern, 1);
n               = prod(res);

if verbose
    fpo('Derive System Matrix: ');
    numOfPrintFeedback = 20;
    printInterval = ceil(linspace(1,numOfPatterns,numOfPrintFeedback+1));
    printInterval = unique(printInterval(2:end));
end

A = zeros([nnz(config.measurementPattern),n]);
AOffset = 0;

for i = 1:numOfPatterns
    
    curCurrentPattern = config.currentPattern(i,:);
    curCoilsCurrents = curCurrentPattern(logical(curCurrentPattern));
    curMeasurementPattern = config.measurementPattern(i,:);
    curNumOfSensors = nnz(curMeasurementPattern);
    
    currentListOfCoils = setup.coils(config.coilsActive(logical(curCurrentPattern)));
    magnFields = createExcitationFields(currentListOfCoils, voxelGrid, false);
    
    currentListOfSensors = setup.sensors(config.sensorsActive(logical(curMeasurementPattern)));
    ARaw = createRelaxationFields(currentListOfSensors, magnFields, voxelGrid, false);
    
    ATemp = zeros([curNumOfSensors,n]);
    for j = 1:numel(ARaw)
        ATemp = ATemp + ARaw{j} .* curCoilsCurrents(j);
    end
    A(AOffset+1:AOffset+curNumOfSensors,:) = ATemp;
    AOffset = AOffset + curNumOfSensors;
    
    if verbose && any(i==printInterval)
        fprintf('#');
    end
end

if verbose
    for i = 1:(numOfPrintFeedback-numel(printInterval))
        fprintf('#');
    end
    fprintf('\n');
end


end