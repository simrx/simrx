%RAWEXPORTSETUP simulates and exports a setup in raw format.
%
%   RAWEXPORTSETUP(setup, rawPath, res) exports a simulates a given setup
%   in a define resolution and exports the raw data into path rawPath.
%
%   RAWEXPORTSETUP(setup, rawPath, res, config) exports only data that is
%   required by to simulate given config.


%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


function rawExportSetup(setup, rawPath, res, varargin)

%% parse inputs
p = inputParser;
p.addRequired('setup', @isstruct);
p.addRequired('rawPath', @ischar);
p.addRequired('res', @isnumeric);
p.addOptional('config', [], @isstruct);
p.addParameter('saveSingleSensors', false, @islogical);
p.parse(setup, rawPath, res, varargin{:});

setup               = p.Results.setup;
rawPath             = p.Results.rawPath;
res                 = p.Results.res;
config              = p.Results.config;
saveSingleSensors   = p.Results.saveSingleSensors;


%%
% make sure that the provided resolution has three entries.
res = [res 0 0 0];
res = res(1:3);
resString = sprintf('%i.%i.%i', res(1), res(2), res(3));
voxelGrid = createVoxelGrid(setup.roi, res);


%%
% if no config is provided, all coils are active one after another
% with all sensors measuring the response
if isempty(config)
    config.coilsActive      = 1:numel(setup.coils);
    config.sensorsActive    = 1:numel(setup.sensors);
    currentPattern          = createPattern(config.coilsActive, 'sequential');
    measurementPattern      = repmat(createPattern(config.sensorsActive, 'simultaneous'), [size(currentPattern, 1), 1]);
else
    currentPattern          = config.currentPattern;
    measurementPattern      = config.measurementPattern;
end


%%
numOfCoils      = numel(config.coilsActive);
numOfSensors    = numel(config.sensorsActive);

% derive map that defines what sensors have to simulated for each coil
% depending on the current and measurement patterns
coilSensorMap   = false(numOfCoils, numOfSensors);
for i = 1:numOfCoils
    coilSensorMap(i,:) = logical(sum(measurementPattern((find(currentPattern(:,i)>0)),:), 1));
end


%%
for i=1:numOfCoils
    
    curCoil = config.coilsActive(i);
    
    curPath = sprintf('%s/%s/coil.%04d', rawPath, resString, curCoil);
    
    if ~exist(curPath, 'dir')
        mkdir(curPath);
    end
    
    todoSingleSensors  = false(1, numOfSensors);
    todoFields         = false;
    todoAllSensors     = false;
    
    if ~exist([curPath,'/allSensors.dat'], 'file')
        todoAllSensors = true;
    end
    if ~exist([curPath,'/magnField.dat'], 'file')
        todoFields = true;
    end
    if saveSingleSensors
        for k=config.sensorsActive(coilSensorMap(i,:))
            if ~exist(sprintf('%s/sensor.%04d.dat',curPath,k), 'file')
                todoSingleSensors(k) = true;
            end
        end
    end
    
    fpo(sprintf('calculate coil %04i (%04i/%04i) [%s]: ',curCoil, i, numOfCoils, resString));
    
    if all(~[todoSingleSensors,todoFields,todoAllSensors])
        fprintf('skipped (exist)\n');
        continue;
    end
    fprintf('\n');
    
    
    %%
    magnFields = createExcitationFields(setup.coils(curCoil), voxelGrid);
    
    if todoFields
        parsaveBinary(sprintf('%s/magnField.dat',curPath), magnFields{1});
    end
    
    if (todoAllSensors || any(todoSingleSensors))
        ARaw = createRelaxationFields(setup.sensors, magnFields, voxelGrid);
        
        if todoAllSensors
            parsaveBinary(sprintf('%s/allSensors.dat',curPath), ARaw{1});
        end
        for k=config.sensorsActive(todoSingleSensors)
            parsaveBinary(sprintf('%s/sensor.%04d.dat',curPath,k), ARaw{1}(k,:));
        end
    end
    
    fpo(sprintf('calculate coil %04i (%04i/%04i) [%s]: ',curCoil, i, numOfCoils, resString));
    fprintf('done\n\n');
    
    
end


end



