%   README.m can be executed to run all scripts to fully compile this
%   setup. Then examples/exampleC.m shows an example how to use this setup.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


if(~isdeployed)
    cd(fileparts(which(mfilename)));
end


%% load SiMRX toolbox
run('../../initSiMRX.m'); % load SiMRX path relative to this example script


%%
decoratedBox('Run scripts for setup ''realistic3D''');

% create a formatted text file that contains setup information
run('scripts/createRawData.m');


% parse rawData into MRX setup file
run('scripts/createSetup.m');


% parse a setup file
run('scripts/createConfigs.m');


%%
% % uncomment the following scripts to see how to handle measured and
% % simulated datasets.
%
% % load and cleanup setup and config
% run('scripts/loadMeasuredData.m');
%
%
% % simulate measurement
% run('scripts/simulateMeasuredData.m');

decoratedBox('setup ''realistic3D'' done');

