% This script creates a SiMRX config for the realistic3D setup from raw
% files.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


if(~isdeployed)
    cd([fileparts(which(mfilename)), '/..']);
end


%% load setup
setup = loadSetup('default');


%% set config
config = [];
config.info.name        = 'all';

config.coilsActive      = 1:size(setup.coils, 2);
config.sensorsActive    = 1:size(setup.sensors, 2);

for filesetID = 1:5
    
    currents                    = load(sprintf('rawData/dataset.%02i.currents.dat', filesetID));
    config.currentPattern       = diag(currents);
    config.measurementPattern   = createPattern(config.sensorsActive, 'simultaneous', 'times', size(config.currentPattern, 1));
    config.info.variant         = sprintf('singleSequential.dataset.%02i', filesetID);
    
    % check compatibility with setup and save config
    if checkCompatibility(setup, config)
        saveConfig(sprintf('configs/%s/%s', config.info.name, config.info.variant), config, 'ff', true);
    end
end

