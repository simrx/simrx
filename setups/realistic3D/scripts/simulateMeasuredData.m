% This script simulates a measurement and compares it with a reference
% measurement loaded from a raw file.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


if(~isdeployed)
    cd([fileparts(which(mfilename)), '/..']);
end


%%
run('scripts/loadMeasuredData.m');


%% simulate system measurement
MNPmg               = 100; %magnetic nanoparticles in mg in the region of interest

% set susceptibility of the MNP
ChiMNP              = 0.0005;


f                   = createPhantom(res, 'P', 'options', struct('XMNP',MNPmg * (ChiMNP * 1e15), 'layer', filesetID));
f                   = f(:).*mu0; %undo the rescale in the real data
gSim                = A * f;

% gNoisy = addGaussianNoiseWithSNR(100, g);


%%
figure;
hold on;
plot(gSim);
plot(gMeas);

