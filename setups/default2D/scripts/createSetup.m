% This script creates SiMRX setup called default2D.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021


tempPath = pwd;

if(~isdeployed)
    cd([fileparts(which(mfilename)), '/..']);
end


%%
cla;
fid = figure('Visible', 'off');


%% create a new 2D setup
setup               = [];
setup.info.name     = 'default2D';

setup.dim           = 2;

setup.roi.x         = [-.05, .05];
setup.roi.y         = [-.05, .05];
setup.roi.z         = [   0,   0];

setup.coils         = [];
setup.coils         = [setup.coils, createEntityArray([ .055, .045, 0], [ .055,-.045, 0], [-1, 0, 0], [ 1,10,1])];
setup.coils         = [setup.coils, createEntityArray([-.055, .045, 0], [-.055,-.045, 0], [ 1, 0, 0], [ 1,10,1])];
setup.coils         = [setup.coils, createEntityArray([ .045, .055, 0], [-.045, .055, 0], [ 0,-1, 0], [10, 1,1])];
setup.coils         = [setup.coils, createEntityArray([ .045,-.055, 0], [-.045,-.055, 0], [ 0, 1, 0], [10, 1,1])];

setup.coilGroups    = {1:10, 11:20, 21:30, 31:40};

setup.sensors       = [];
setup.sensors       = [setup.sensors, createEntityArray([ .050, .060, 0], [-.050, .060, 0], [-1,-1, 0], 10)];
setup.sensors       = [setup.sensors, createEntityArray([ .050, .060, 0], [-.050, .060, 0], [ 1,-1, 0], 10)];
setup.sensors       = [setup.sensors, createEntityArray([ .050, .065, 0], [-.050, .065, 0], [ 0,-1, 0], 10)];
setup.sensors       = [setup.sensors, createEntityArray([ .050, .065, 0], [-.050, .065, 0], [ 0, 1, 0], 10)];
setup.sensors       = [setup.sensors, createEntityArray([ .050, .065, 0], [-.050, .065, 0], [ 1, 0, 0], 10)];
setup.sensors       = [setup.sensors, createEntityArray([ .050, .065, 0], [-.050, .065, 0], [-1, 0, 0], 10)];
setup.sensors       = [setup.sensors, createEntityArray([ .030, .080, 0], [-.030, .080, 0], [ 0,-1, 0],  8)];
setup.sensors       = [setup.sensors, createEntityArray([-.070, .060, 0], [-.020, .110, 0], [ 1,-1, 0],  8)];
setup.sensors       = [setup.sensors, createEntityArray([ .070, .060, 0], [ .020, .110, 0], [-1,-1, 0],  8)];

setup.sensorGroups  = {1:20, 21:60, 61:68, 69:76, 77:84};


%% save setup variants
setup.info.variant  = 'default';
saveSetup(setup.info.variant, setup, 'ff', true);
cla;visualizeMRX(setup, 'fid', fid, 'export', setup.info.variant);


%%
cd(tempPath);

