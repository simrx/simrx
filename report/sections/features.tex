%!TEX root = ../main.tex
\section{Features and Modules}\label{sec:features}
The \simrx toolbox is separated in the following submodules (each in its own subfolder):

\begin{mymarginbox}	
	\begin{tabular}{@{}L{\textwidth}@{}}%
		\mypath{./configuration}\\
		\mypath{./simulation}\\
		\mypath{./phantom}\\
		\mypath{./visualization}
	\end{tabular}
\end{mymarginbox}

We give a short overview of each module in the following sections.
For all provided functions, in detail information of syntax and features are available in each file header.
This documentation is also available using the MATLAB help function (F1).


\subsection{Configuration}\label{ss:config}
\begin{figure}
	\begin{mymarginbox}
		\begin{lstfloat}[H]
			\lstinputlisting[style=matstyle]{code/setup.m}
			\caption[2D setup]{MATLAB code to create a 2D setup called \emph{2Dsetup}. The region of interest is defined as a 2D square with a side length of 10\si{cm}. Ten activation coils are positioned in front of each the edge of the region of interest. Each sides coils are collected into a unique coil group, which lead to four coil groups. A total of 20 sensors are positioned above the region of interest and have an angled orientation. The different orientations are grouped accordingly as sensor groups. Since no alternations to the coils are done, the 2D default model of the coils is used, hence \emph{default} is chosen as variant name.}
			\label{code:2Dsetup}
		\end{lstfloat}
	\end{mymarginbox}
\end{figure}
\begin{figure}
	\begin{mymarginbox}
		\begin{lstfloat}[H]
			\lstinputlisting[style=matstyle]{code/config.m}
			\caption[2D config]{MATLAB code to create a config suitable for the setup created in \cref{code:2Dsetup}. The configuration name is set to \emph{all} since it uses all coils and sensors of the corresponding setup. Then a subsequent current pattern, which only one coil active at a time is implemented. Here all sensors are active for every activation. This config variant is named \emph{singleSequential}.}
			\label{code:2Dconfig}
		\end{lstfloat}
	\end{mymarginbox}
\end{figure}
This module provides a set of functions to create \texttt{setup} and \texttt{config} files.
\paragraph{Conventions for 2D Configurations:}\label{para:config}
\simrx supports 2D and 3D systems.
However, at its core, the toolbox is designed for 3D simulations.
Therefore 2D setups are implemented as 3D setups with the following adjustments:
\begin{itemize}
\item all entities are on the same z-layer (z=0 recommended)
\item the region of interest in z direction is set accordingly (\texttt{setup.roi.z = [0,0]} recommended)
\item the voxel resolution \texttt{res} in z direction is set to 1, e.g. \texttt{res = [x,y,1]};
\item coils \emph{must be} represented as dipole magnets to be in line with \cref{ss:coilDiscretization}. This means that \texttt{coils.Segments} will be ignored, regardless of availability.
\end{itemize}
Also consider the provided 2D and 3D setups as part of \simrx (see \cref{subs:sytheticdataset}).


\subsubsection{Setup file}\label{sss:setup}
In MATLAB a \texttt{setup} file is represented as struct.
It holds the following fields

\begin{mymarginbox}%
	\begin{tabular}{l}%
		\texttt{info},\quad\texttt{dim},\quad\texttt{roi},\quad\texttt{coils},\quad\texttt{coilGroups},\quad\texttt{sensors}\quad and\quad\texttt{sensorGroups}.
	\end{tabular}
\end{mymarginbox}

As of version \version\ \texttt{setup.info} struct only contains the name of the setup, as well as the current variant name (for setup variants, see \cref{ss:setupfolderstructure}).

Determined by the dimension of the system, \texttt{setup.dim} is either 2 or 3.
The region of interest \texttt{setup.roi} is a struct with fields \texttt{x}, \texttt{y} and \texttt{z}, that holds boundary information in an array with unit meter $\left[\si{m}\right]$. For instance \texttt{setup.roi.x = [0,0.1]} translates to a region of interest of $10\si{cm}$ length in x direction.
Yet again \texttt{setup.coils} and \texttt{setup.sensors} are arrays of structs with fields:

\begin{mymarginbox}%
	\begin{tabular}{@{}L{0.30\textwidth}@{}L{0.30\textwidth}@{}}%
		\texttt{coils(i).Position},&\texttt{sensors(i).Position},\\
		\texttt{coils(i).Normal},&\texttt{sensors(i).Normal},\\
		\texttt{coils(i).Segments},
	\end{tabular}
\end{mymarginbox}

where $i$ is the $i$-th coil/sensor.
As mentioned before, spatial information (in this case the fields \texttt{Position} and \texttt{Normal}) are given as a three element vector with base unit meter $\left[\si{m}\right]$.

The function \texttt{createEntityArray.m} is used to create new coil and sensor entries.
In the current version the function takes two outer points, a normal and the number of intermediate points.
Then \texttt{createEntityArray.m} returns a list of equidistant points, that represent the entity position, as well as the respective normal.

The setup format allows to group coil and sensors.
By this the creation of config files for certain partitions of a setup can be streamlined.
This is realized by the cell arrays \texttt{setup.coilGroups} and \texttt{setup.sensorGroups}, where each cell element contains a list of coil and sensor IDs respectively.

In the 3D case the coil wire is modeled by a list of points, which represent the coil shape.
The function \texttt{createCoilLoop.m} creates a coil template, which has to be copied in every coil position.
The function \texttt{parseCoils.m} receives a coil struct (with fields \texttt{Position} and \texttt{Normal}) and adds the \texttt{Segments} field in respect to the given coil template.
Internally this uses the function \texttt{relocateStructure.m} that reorientates a given list of points.

In case a predefined voxel grid is given, the corresponding region of interest can be determined using \texttt{getROI.m}. During the creation of a new \texttt{setup} the usage of \texttt{visualizeSetup.m} is recommended.

The script in \cref{code:2Dsetup} can be used to create the \texttt{setup} file for the 2D system shown in \cref{im:vis}.
Please also examine the examples given in \cref{ss:examplesetups}, that make use of the described functionalities.


\subsubsection{Config file}\label{sss:config}
\texttt{config} files are, similar to \texttt{setup} files, structs in MATLAB. They hold the fields

\begin{mymarginbox}
	\begin{tabular}{l}%
		\texttt{info},\quad\texttt{coilsActive},\quad\texttt{sensorsActive},\quad\texttt{currentPattern}\quad and\ \texttt{measurementPattern}.
	\end{tabular}
\end{mymarginbox}

As for the \texttt{setup} struct, the \texttt{config.info} struct only contains the name of the config, as well as the current variant name (for config variants, see \cref{ss:setupvariants}).

A setup may contain a lot of coils/sensors that are not used in a specific configuration.
To optimize the simulation speed \texttt{coilsActive} and \texttt{sensorsActive} are lists of coil and sensor ids, that define the subset of active setup parts.

The coil current sequence is stored in \texttt{config.currentPattern} as a matrix, where each column corresponds to a coil.
Each row represents one pattern, where the entries refer to the applied current at the respective coil.
The measurement pattern is stored in \texttt{config.measurementPattern} in binary matrix form.
Here each column corresponds to a sensor, which is active during the current measurement depending on the binary entry.
The function \texttt{createPattern.m} provides standard patterns and multiple other presets and can be used to even-handedly create current and measurement patterns.

The script in \cref{code:2Dconfig} can be used to create a \texttt{config} file for the 2D system shown in \cref{im:vis}.
Again please examine the examples given in \cref{ss:examplesetups}, that make use of the described functionalities.


\subsubsection{Setup and Config Validation}\label{sss:scvalid}
This toolbox also contain functions to check the created \texttt{setup} and \texttt{config}.
The functions \texttt{isConfigValid.m} and \texttt{isSetupValid.m} validate if all required fields are set and further include checks for data inconsistencies.
Additionally the function \texttt{checkCompatibility.m} checks if a given \texttt{config} is applicable for a given \texttt{setup}.


\subsubsection{Save and Load}
The \texttt{setup} and \texttt{config} datasets can be stored using \texttt{saveSetup.m} and \texttt{saveConfig.m}.
These are stored using a custom \texttt{.mrxsetup} and \texttt{.mrxcfg} extension, that is based on the MATLAB internal \texttt{.mat} file format.
Corresponding load functions (\texttt{loadSetup.m} and \texttt{loadConfig.m}) are available.

To store the created \texttt{setup} and \texttt{config} datasets, we recommend using the folder structure proposed in \cref{ss:setupfolderstructure}.


\subsection{Simulation}
\label{ssec:simulation}
\begin{figure}[t]
	\begin{mymarginbox}
	\includegraphics[width=\textwidth, page=2]{images/systemmatrix}
	\end{mymarginbox}
	\caption{Schematics of the pattern-based system matrix creation workflow. Variables are marked with a rounded corner box, functions are marked with a gray box. The variable usage is color coded: green (dashed) as input and red (solid) as output argument.}
	\label{im:systemmatrixPattern}
\end{figure}
\begin{figure}[t]
	\begin{mymarginbox}
	\includegraphics[width=\textwidth, page=1]{images/systemmatrix}
	\end{mymarginbox}
	\caption{Schematics of the coil-based system matrix creation workflow using raw files internally. Variables are marked with a rounded corner box, functions are marked with a gray box. The variable usage is color coded: green (dashed) as input and red (solid) as output argument.}
	\label{im:systemmatrixCoil}
\end{figure}
The simulation module is the core element of the toolbox.
All necessary files can be found in the folder \mypath{./simulation}.
The main script is provided by the file \texttt{createSystemMatrix.m}.
It processes a given MRX setup and configuration into a linear operator, namely a matrix \texttt{A}.
A valid \texttt{setup} file is required, that provides information about dimension, region of interest, coil position/orientation and sensor position/orientation.
Furthermore lists of active coils and sensors as well as coil current and measurement patterns are provided by a \texttt{config} file.
For the simulation step we require to provide the desired voxel/pixel resolution.
See \cref{sss:scvalid} for tools to validate the setup and config pair.
By default the function \texttt{createSystemMatrix.m} internally calls \texttt{createSystemMatrixByPattern.m}.
Alternatively \texttt{createSystemMatrix.m} can be requested to call the function \texttt{createSystemMatrixByCoil.m} instead.
Both processes create the same system matrix, however it is performed in different ways:
The function \texttt{createSystemMatrixByPattern.m} derives the system matrix one pattern after another.
It combines the active coils magnetic fields with the contemporary coil current on the fly and derives the resulting dipole fields for the final system matrix.
On the other hand \texttt{createSystemMatrixByCoil.m} derives a base magnetic field with a unitary coil current for each coil individually.
Then for each of these fields the resulting dipole fields are calculated and stored as a \emph{raw measurement}.
Due to linearity of the system, the final system matrix can be derived as linear combination of these raw measurements.
These processes are illustrated in \cref{im:systemmatrixPattern} and \cref{im:systemmatrixCoil}.

The internal structure of the \texttt{createSystemMatrixByPattern.m} routine is shown in \cref{im:systemmatrixPattern}.
First a valid \texttt{voxelGrid} is created using \texttt{createVoxelGrid.m}.
Then for each pattern we derive the currently active coils with a activation current according to \texttt{config.currentPattern}.
The compiled list of coils is used in \texttt{createExcitationFields.m} to calculate the resulting magnetic field for this current pattern.
Internally the choice of the used mathematical model depends on the existence of \texttt{coils.Segments} (see \cref{ss:coilDiscretization}).
Finally \texttt{createRelaxationFields.m} translates the derived field on the voxel grid into the magnetic dipole response.
However the evaluation of the magnetic response is only performed on the list of active sensors in respect to \texttt{config.measurementPattern}.
A composition of all patterns lead to the fill system matrix $A$.

\Cref{im:systemmatrixCoil} shows the internal structure of the \texttt{createSystemMatrixByCoil.m} routine.
In the same way \texttt{createVoxelGrid.m} creates a \texttt{voxelGrid} first.
Then \texttt{createExcitationFields.m} is called to calculate the magnetic field for each coil individually with a unitary current.
The base magnetic fields are feed into \texttt{createRelaxationFields.m}.
This function translates the fields present on the voxel grid into the magnetic dipole fields, which is acquired by the sensors.
As a result we receive a \emph{raw measurement} for each coil, which has been created by a single coil with unitary current.
Due to the linearity of the forward operator, we can now apply a current and measurement pattern by using \texttt{applyCurrentPattern.m}.

We see that these procedures have it pros and cons.
The default derivation by pattern is usually faster, since we do not have any overhead in deriving and storing the raw system matrices corresponding to single coil activations.
Depending on the activation and measurement pattern, the function \texttt{createSystemMatrixByCoil.m} may do a lot of unnecessary work.
On the other side however, when for example only small parts of the system are changed between experiments, the function \texttt{createSystemMatrixByPattern.m} recalculates every signal from the ground up, whereas the function \texttt{createSystemMatrixByCoil.m} is able to reuse the raw measurements and only requires to reevaluate the current and measurement patterns.
Given that storage capacity is available, it may often be an advantage to use previously calculated raw files instead, as described in the next section.

\subsubsection{Raw export/import}\label{sss:rawExIm}
As stated in the end of the previous section, it can be advantageous to use precalculated raw files, since the simulation of the setup is a time demanding step in most cases.
The \simrx toolbox provides tools to dump \texttt{ARaw} files into file.
As seen in \cref{im:systemmatrixCoil}, \texttt{ARaw} dataset are preliminary files in the system matrix creation process.
Storing these separately allows for flexible experimentation with varying coil currents.
Note that this procedure is not very suitable in case the set of active coils or sensors changes regularly, since \texttt{ARaw} contains a fixed set of precalculated coil results.

The function \texttt{exportRawSetup.m} allows for in batch export of single coil and sensor data, whereas \texttt{importRawSetup.m} allows to import previously derived raw files.
Internally the simulation steps done in \texttt{exportRawSetup.m} are equivalent to those steps done by \texttt{createSystemMatrixByCoil.m}.
A working example (\texttt{ExampleB.m}) is presented in \cref{s:examples}.


\subsection{Phantom}\label{ss:phantom}
\begin{figure}%
	\begin{mymarginbox}
	\includegraphics[width=\textwidth]{images/phantoms}
	\end{mymarginbox}
	\caption{Selection of available 3D phantoms with a resolution of $[50,50,15]$. One phantom per row, each column represents a layer. Layers $1, 2, 14$ and $15$ are cut from illustration, due to clarity. Phantom names from top to bottom: \texttt{'F\_2'},\texttt{'shepplogan3d'},\texttt{'tumor'} and \texttt{'fwhmdots\_0.25'}.}
	\label{im:phantom}
\end{figure}
The function \texttt{createPhantom.m} provides multiple phantom options, including phantoms suited for reconstruction or resolution tests.
\texttt{createPhantom.m} uses the function \texttt{phantom3.m}, which is a 3D reimplementation of the MATLAB given phantom function and able to generate 3D phantoms that are composed of multiple ellipses.
For all available phantoms, please check the help section of \texttt{createPhantom.m} and \texttt{phantom3.m}. See \cref{im:phantom} for a selection of the available phantoms.



\subsection{Visualization}\label{ss:vis}
\begin{figure}
	\begin{mymarginbox}
		\includegraphics[width=\textwidth]{images/visDefault2D}\\
		\includegraphics[width=\textwidth]{images/visDefault3D}\\
		\includegraphics[width=\textwidth]{images/visRealistic3D}
	\end{mymarginbox}
	\caption{Visualization of the setups \emph{'default2D'} (top row), \emph{'default3D'} (middle row) and \emph{'realistic3D'} (bottom row). The left column shows the respective setup visualization. In the middle column one coil is active and the corresponding magnetic field is visualized. Here the setups \emph{default2D} and \emph{realistic3D} are visualizing the magnetic fields as arrows, whereas \emph{default3D} shows a volume visualization of the fields magnetic field strength in each voxel. The right column shows the setup with an embedded phantom. The \emph{default2D} and \emph{default3D} setups are using a simple \texttt{'F'} phantom in 2D and 3D respectively. The \emph{realistic3D} setup is using a \texttt{'P'} phantom, where the P is positioned in the bottom layer. Coils are colored red, sensors green, magnetic fields blue.}
	\label{im:vis}
\end{figure}
\simrx provides tools to visualize the region of interest (\texttt{drawROI.m}), coils (\texttt{drawCoils.m}), sensors (\texttt{drawSensors.m}), magnetic fields (\texttt{drawField.m}) and 3D volumes (\texttt{drawVolume.m}).

The function \texttt{visualizeMRX.m} is an wrapper that combines all of these functions into a single visualization tool.
It is equipped with multiple hotkeys that allows dynamic analysis of a given system.
With \texttt{visualizeMRX.m} we are able to create live previews of applied current patterns and visualize these fields with arrow fields or intensity maps.
Furthermore we can dynamically switch of each visualization module, e.g. to hide sensor arrays. 
The visualizations shown in \cref{im:vis} are all created using \texttt{visualizeMRX.m} only.
The figures can be recreated using the code provided in \mypath{./examples/visualization.m}.



