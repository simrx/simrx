%ISOCTAVE returns true if executed in octave, false otherwise.


%   Author:     Lea Föcke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       19-May-2021


function out = isOctave()
out = exist('OCTAVE_VERSION', 'builtin') ~= 0;
end