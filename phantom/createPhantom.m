%CREATEPHANTOM creates commonly used phantoms for magnetorelaxometry
%   experiments with given resolution and particle amount.
%
%   run:
%       CREATEPHANTOM
%
%   to see a minimal example.
%
%	Syntax:
%		out = createPhantom(varargin)
%
%       REQUIRED:
%       =========
%		res:            Array [x1,x2,x3] that defines the volume spatial
%						resolution
%		shape:          String specifying the phantom. See list below.
%
%       OPTIONAL:
%       =========
%       options:        struct that contains the following fields:
%           interpMethod:	Specify the interpolation method for rescaling.
%						The default is linear interpolation. Available are:
%						'nearest' - nearest neighbor interpolation
%						'linear'  - linear interpolation
%						'spline'  - spline interpolation
%						'cubic'   - cubic interpolation
%           XMNP:       numerica value of the total amount of particles in
%                       the phantom
%           additionally there are several modifier fields, that changes
%           the properties of a certain phantom. Check the phantoms code
%           for details about the available modifiers.
%       showPhantom:    true or false, shows the requested phantom in a new
%                       figure. The 3D visualization shows the z-layer that
%                       *approximately* represents the 2D slice. However,
%                       due to approximation errors, this can be off by a
%                       slide in any direction.
%
%   Each phantom type has an own version number. Scripts may check this
%   version number to avoid inconsistancies, after a phantom has been
%   updated.


%	IMPORTANT CHANGE:
%	v1.1:	XMNP defines now the total amount of particles in the phantom,
%			it is no longer the particle amount per voxel of the default
%			size that was [10,10,5].
%			By scaling the total amount of particles is not changed

%   Author:     Lea Foecke
%   E-Mail:     lea.foecke@fau.de
%   Institute:  FAU Erlangen-Nürnberg
%   Date:       21-May-2021

%   Original version (v1.0):
%   Author:     Lea Foecke (*) and Peter Schier (**)
%   E-Mail:     (*)  lea.foecke@fau.de
%				(**) peter.schier@umit.at
%   Institute:  (*)  FAU Erlangen-Nürnberg
%				(**) UMIT, Hall in Tirol
%   Date:       24-Nov-2017


function [out, uniquePhantomName] = createPhantom(varargin)


%% run example?
if nargin == 0
    out = runExample;
    return;
end


%% quick parse phantom struct
if numel(varargin)==1 && isstruct(varargin{1})
    vararginS = varargin{1};
    
    if isfield(vararginS, 'res')
        varargin{2} = vararginS.res;
    else
        error('phantom resolution required');
    end
    
    if isfield(vararginS, 'shape')
        varargin{2} = vararginS.shape;
    else
        error('phantom shape required');
    end
    
    if isfield(vararginS, 'options')
        varargin{3} = 'options';
        varargin{4} = vararginS.options;
    end
    
    if isfield(vararginS, 'showPhantom')
        varargin{5} = 'showPhantom';
        varargin{6} = vararginS.showPhantom;
    end        
end


%% parse inputs
p = inputParser;
p.addRequired('resolution');
p.addRequired('shape');
p.addParameter('options', [], @isstruct);
p.addParameter('showPhantom', false, @islogical);

p.parse(varargin{:});

res   	= p.Results.resolution;
shape           = p.Results.shape;
options         = p.Results.options;
showPhantom     = p.Results.showPhantom;

dim = numel(res);
if dim == 2
    res = [res,1];
end
if dim == 3 && res(3) == 1
    dim = 2;
end

if isempty(options) || ~isfield(options, 'XMNP')
    options.XMNP = 1;
end
XMNP = options.XMNP;


%% create phantom
switch shape
    case 'P'
        vers = '0.2';
        
        % P shape in the third of five layers.
        
        % Modifiers:
        % layer     changes the z layer of the object layer in 1-5
        %
        % phantom is created on resolution [10,10,5] and scaled down to
        % requested resolution
        
        layer = 0; % layer
        if isfield(options, 'layer'); layer = options.layer; end
        layer = max(min(layer, 5), 1);
        
        
        out = zeros(10,10,5);
        out(4:7,5:8,layer) 	= 1;
        out(5:6,6:7,layer) 	= 0;
        out(4,2:4,layer) 	= 1;
        
        
        needScaling         = true;
        slice2D             = layer;
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'nearest'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_v%s', shape, XMNP, options.interpMethod, layer, vers);
        
        
    case 'PInv'
        vers = '0.2';
        
        % Inverted P shape in the third of five layers.
        
        % Modifiers:
        % layer      changes the z layer of the object layer in 1-5
        %
        % phantom is created on resolution [10,10,5] and scaled down to
        % requested resolution
        
        layer = 0; % layer
        if isfield(options, 'layer'); layer = options.layer; end
        layer = max(min(layer, 5), 1);
        
        
        out = zeros(10,10,5);
        out(4:7,3:6,layer)	= 1;
        out(5:6,4:5,layer) 	= 0;
        out(7,7:9,layer) 	= 1;
        
        
        needScaling         = true;
        slice2D             = layer;
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'nearest'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_v%s', shape, XMNP, options.interpMethod, layer, vers);
        
        
    case 'F'
        vers = '0.2';
        
        % F shape in the third of five layers.
        
        % Modifiers:
        % layer      changes the z layer of the object layer in 1-5
        %
        % phantom is created on resolution [10,10,5] and scaled down to
        % requested resolution
        
        layer = 0; % layer
        if isfield(options, 'layer'); layer = options.layer; end
        layer = max(min(layer, 5), 1);
        
        
        out = zeros(10,10,5);
        out(4,3:8,layer)    = 1;
        out(5:6,5,layer)    = 1;
        out(5:7,8,layer)    = 1;
        
        
        needScaling         = true;
        slice2D             = layer;
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'nearest'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_v%s', shape, XMNP, options.interpMethod, layer, vers);
        
        
    case 'grad'
        vers = '0.2';
        
        % gradient image with six gradient steps
        
        % Modifiers:
        % no modifiers available
        %
        % phantom is created on resolution [10,10,5] and scaled down to
        % requested resolution
        
        
        out                 = zeros(10,10,5);
        out(7,3:9,5)        = 1;
        out(4,3:6,5)        = 1;
        out(5:6,3,5)        = 1;
        out(5:6,6,5)        = 1;
        
        out(9,3:5,1)        = 1;
        out(2,3:5,1)        = 1;
        out(8,6:7,1)        = 1;
        out(3,6:7,1)        = 1;
        out(7,8:9,1)        = 1;
        out(4,8:9,1)        = 1;
        out(5:6,6:7,1)      = 1;
        
        out(3:8,3,2)        = 6*1/6;
        out(3:8,4,2)        = 5*1/6;
        out(3:8,5,2)        = 4*1/6;
        out(3:8,6,2)        = 3*1/6;
        out(3:8,7,2)        = 2*1/6;
        out(3:8,8,2)        = 1*1/6;
        
        out(3,3:8,4)        = 6*1/6;
        out(4,3:8,4)        = 5*1/6;
        out(5,3:8,4)        = 4*1/6;
        out(6,3:8,4)        = 3*1/6;
        out(7,3:8,4)        = 2*1/6;
        out(8,3:8,4)        = 1*1/6;
        
        out(3:8,3:8,3)      = 1/2;
        out(4:7,4:7,3)      = 1;
        out(5:6,5:6,3)      = 0;
        
        
        needScaling         = true;
        slice2D             = 4;
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'nearest'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_v%s', shape, XMNP, options.interpMethod, vers);
        
        
    case 'shepplogan3d'
        vers = '0.2';
        
        % Shepp Logan phantom
        
        % Modifiers:
        % rotation   rotation of the phantom around z axis in degree
        %
        % phantom is created on resolution
        % [max(resolution), max(resolution), max(resolution)] and
        % scaled down to requested resolution
        
        rotation = 0;
        if isfield(options, 'rotation'); rotation = options.rotation; end
        
        
        e =    [
            1.0  .6900  .920  .810     0       0     0    0+rotation  0   0
            -.8  .6624  .874  .780     0  -.0184     0    0+rotation  0   0
            -.2  .1100  .310  .220   .22       0     0  -18+rotation  0  10
            -.2  .1600  .410  .280  -.22       0     0   18+rotation  0  10
            .1  .2100  .250  .410     0     .35  -.15    0+rotation  0   0
            .1  .0460  .046  .050     0      .1   .25    0+rotation  0   0
            .1  .0460  .046  .050     0     -.1   .25    0+rotation  0   0
            .1  .0460  .023  .050  -.08   -.605     0    0+rotation  0   0
            .1  .0230  .023  .020     0   -.606     0    0+rotation  0   0
            .1  .0230  .046  .020   .06   -.605     0    0+rotation  0   0];
        out                 = phantom3(e, max(res));
        out                 = flip(permute(flip(out,2), [2,1,3]),1);
        
        
        needScaling         = true;
        slice2D             = round(max(res)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_v%s', shape, XMNP, options.interpMethod, rotation, vers);
        
        
    case 'tumor'
        vers = '0.2';
        
        % phantom of a tumor
        
        % Modifiers:
        % densityA      color value of the main object, default 0.8
        % densityB      color value of the vein, default 0.3
        % densityC      color value of the encapsulation, default 1
        %
        % phantom is created on resolution
        % [max(resolution), max(resolution), max(resolution)] and
        % scaled down to requested resolution
        
        densityA = 0.8;
        if isfield(options, 'densityA'); densityA = options.densityA; end
        densityB = 0.3;
        if isfield(options, 'densityB'); densityB = options.densityB; end
        densityC = 1;
        if isfield(options, 'densityC'); densityC = options.densityC; end
        
        
        e =    [ -1  .6900   .620  .900    0    0    0   0   0    0
            -2      1	  .1    .2  -.7    0    0  30   0    0
            -4      1	  .1    .2  1.0  -.1  -.1   0  30   50
            -8      1	  .1    .2  1.0  -.1  -.1   0  30  -50
            -16   .35  .15  .25     0      -0.38   +.15    0  0   0];
        out                 = phantom3(e, max(res));
        out(out <= -16)     = densityC;
        out(out <= -8)      = densityB;
        out(out <= -4)      = densityB;
        out(out <= -2)      = densityB;
        out(out <= -1)      = densityA;
        mask                = phantom3(e(1,:), max(res));
        out(~mask)          = 0;
        out                 = flip(permute(flip(out,2), [2,1,3]),1);
        
        
        needScaling         = true;
        slice2D             = round(max(res)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_%g_%g_v%s', shape, XMNP, options.interpMethod, densityA, densityB, densityC, vers);
        
        
    case 'tumorGradient'
        vers = '0.2';
        
        % phantom of a tumor with a gradient in the main object
        
        % Modifiers:
        % densityA1     color value of top of the main object, default 1
        % densityA2     color value of bottom of main object, default 0.3
        % densityB  	color value of the vein, default 0.3
        % densityC      color value of the encapsulation, default 1
        %
        % phantom is created on resolution
        % [max(resolution), max(resolution), max(resolution)] and
        % scaled down to requested resolution
        
        densityA1 = 1;
        if isfield(options, 'densityA1'); densityA1 = options.densityA1; end
        densityA2 = 0.3;
        if isfield(options, 'densityA2'); densityA2 = options.densityA2; end
        densityB = 0.3;
        if isfield(options, 'densityB'); densityB = options.densityB; end
        densityC = 1;
        if isfield(options, 'densityC'); densityC = options.densityC; end
        
        
        e =    [ -1  .6900   .620  .900    0    0    0   0   0    0
            -2      1	  .1    .2  -.7    0    0  30   0    0
            -4      1	  .1    .2  1.0  -.1  -.1   0  30   50
            -8      1	  .1    .2  1.0  -.1  -.1   0  30  -50
            -16   .35  .15  .25     0      -0.38   +.15    0  0   0];
        out                 = phantom3(e, max(res));
        out(out <= -16)     = densityC;
        out(out <= -8)      = densityB;
        out(out <= -4)      = densityB;
        out(out <= -2)      = densityB;
        g = zeros(max(res), max(res), max(res));
        for i = 1:max(res)
            for j = 1:max(res)
                g(max(res)-i+1,1:max(res),j) = (i+j-1)/(2*max(res));
            end
        end
        g                   = densityA1 + (g .* (densityA2-densityA1));
        out(out==-1)        = g(out==-1);
        mask                = phantom3(e(1,:), max(res));
        out(~mask)          = 0;
        out                 = flip(permute(flip(out,2), [2,1,3]),1);
        
        
        needScaling         = true;
        slice2D             = round(max(res)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_%g_%g_%g_v%s', shape, XMNP, options.interpMethod, densityA1, densityA2, densityB, densityC, vers);
        
        
    case 'tumorSimple'
        vers = '0.2';
        
        % simplified phantom of a tumor
        
        % Modifiers:
        % no modifiers available
        %
        % phantom is created on resolution
        % [max(resolution), max(resolution), max(resolution)] and
        % scaled down to requested resolution
        
        
        e =    [    1  .6900   .620  .900    0    0    0   0   0    0
            -1      1	  .1    .2  -.7    0    0  30   0    0
            -1      1	  .1    .2  1.0  -.1  -.1   0  30   50
            -1      1	  .1    .2  1.0  -.1  -.1   0  30  -50 ];
        out                 = phantom3(e, max(res));
        out(out < 0)        = 0;
        out                 = flip(permute(flip(out,2), [2,1,3]),1);
        
        
        needScaling         = true;
        slice2D             = round(max(res)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName  	= sprintf('%s_%g_%s_v%s', shape, XMNP, options.interpMethod, vers);
        
        
    case 'fwhmLineY'
        vers = '0.2';
        
        % two vertical line next to another
        
        % Modifiers:
        % gap           distance of two lines in pixel
        % thickness  	thickness of lines
        
        gap = 2;
        if isfield(options, 'gap'); gap = options.gap; end
        thickness = 2;
        if isfield(options, 'thickness'); thickness = options.thickness; end
        
        
        out = zeros(res);
        gap = max(0, min(gap, res(1)-2*thickness));
        out(	floor(floor(res(1)/2)-(gap/2)-thickness)+1:floor(floor(res(1)/2)-(gap/2)), ...
            floor(res(2)/5):floor((4*res(2)/5)+1), ...
            max(1,floor(res(3)/5)):max(res(3), floor((4*res(3)/5)+1))) = 1;
        out(    floor(floor(res(1)/2)+(gap/2))+1:floor(floor(res(1)/2)+(gap/2)+thickness), ...
            floor(res(2)/5):floor((4*res(2)/5)+1), ...
            max(1,floor(res(3)/5)):max(res(3), floor((4*res(3)/5)+1))) = 1;
        
        
        needScaling         = false;
        slice2D             = round(res(3)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_%g_v%s', shape, XMNP, options.interpMethod, gap, thickness, vers);
        
        
    case 'fwhmLongLineY'
        vers = '0.2';
        
        % two vertical line next to another
        
        % Modifiers:
        % gap           distance of two lines in pixel
        % thickness 	thickness of lines
        
        gap = 2;
        if isfield(options, 'gap'); gap = options.gap; end
        thickness = 2;
        if isfield(options, 'thickness'); thickness = options.thickness; end
        
        
        out = zeros(res);
        gap = max(0, min(gap, res(1)-2*thickness));
        out(floor(floor(res(1)/2)-(gap/2)-thickness)+1:floor(floor(res(1)/2)-(gap/2)),:,:) = 1;
        out(floor(floor(res(1)/2)+(gap/2))+1:floor(floor(res(1)/2)+(gap/2)+thickness),:,:) = 1;
        
        
        needScaling         = false;
        slice2D             = round(res(3)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_%g_v%s', shape, XMNP, options.interpMethod, gap, thickness, vers);
        
        
    case 'fwhmLineX'
        vers = '0.2';
        
        % two vertical line next to another
        
        % Modifiers:
        % gap           distance of two lines in pixel
        % thickness     thickness of lines
        
        gap = 2;
        if isfield(options, 'gap'); gap = options.gap; end
        thickness = 2;
        if isfield(options, 'thickness'); thickness = options.thickness; end
        
        
        out = zeros(res);
        gap = max(0, min(gap, res(2)-2*thickness));
        out(	floor(res(1)/5):floor((4*res(1)/5)+1), ...
            floor(floor(res(2)/2)-(gap/2)-thickness)+1:floor(floor(res(2)/2)-(gap/2)), ...
            max(1,floor(res(3)/5)):max(res(3), floor((4*res(3)/5)+1))) = 1;
        out(    floor(res(1)/5):floor((4*res(1)/5)+1), ...
            floor(floor(res(2)/2)+(gap/2))+1:floor(floor(res(2)/2)+(gap/2)+thickness), ...
            max(1,floor(res(3)/5)):max(res(3), floor((4*res(3)/5)+1))) = 1;
        
        
        needScaling         = false;
        slice2D             = round(res(3)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_%g_v%s', shape, XMNP, options.interpMethod, gap, thickness, vers);
        
        
    case 'fwhmLongLineX'
        vers = '0.2';
        
        % two vertical line next to another
        
        % Modifiers:
        % gap           distance of two lines in pixel
        % thickness     thickness of lines
        
        gap = 2;
        if isfield(options, 'gap'); gap = options.gap; end
        thickness = 2;
        if isfield(options, 'thickness'); thickness = options.thickness; end
        
        
        out = zeros(res);
        gap = max(0, min(gap, res(2)-2*thickness));
        out(:,floor(floor(res(2)/2)-(gap/2)-thickness)+1:floor(floor(res(2)/2)-(gap/2)),:) = 1;
        out(:,floor(floor(res(2)/2)+(gap/2))+1:floor(floor(res(2)/2)+(gap/2)+thickness),:) = 1;
        
        
        needScaling         = false;
        slice2D             = round(res(3)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_%g_v%s', shape, XMNP, options.interpMethod, gap, thickness, vers);
        
        
    case 'pixel'
        vers = '0.2';
        
        % a single pixel in the roi
        
        % Modifiers:
        % xshift    shift in x direction
        % yshift    shift in y direction
        % zshift    shift in z direction
        
        xshift = 0;
        if isfield(options, 'xshift'); xshift = options.xshift; end
        yshift = 0;
        if isfield(options, 'yshift'); yshift = options.yshift; end
        zshift = 0;
        if isfield(options, 'zshift'); zshift = options.zshift; end
        
        
        out = zeros(res);
        posX = max(1,min(res(1),floor((0.5+xshift).*res(1))));
        posY = max(1,min(res(2),floor((0.5+yshift).*res(2))));
        posZ = max(1,min(res(3),floor((0.5+zshift).*res(3))));
        out(posX, posY, posZ) = 1;
        out(out < 0) = 0;
        
        
        needScaling         = false;
        slice2D             = round(res(3)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%g_%g_%g_v%s', shape, xshift, yshift, zshift, XMNP, vers);
        
        
    case 'sphere'
        vers = '0.2';
        
        % simple sphere shape in the middle
        
        % Modifiers:
        % xshift    shift in x direction
        % yshift    shift in y direction
        % zshift    shift in z direction
        % radius    radius of the sphere
        %
        % phantom is created on resolution
        % [max(resolution), max(resolution), max(resolution)] and
        % scaled down to requested resolution
        
        xshift = 0;
        if isfield(options, 'xshift'); xshift = options.xshift; end
        yshift = 0;
        if isfield(options, 'yshift'); yshift = options.yshift; end
        zshift = 0;
        if isfield(options, 'zshift'); zshift = options.zshift; end
        radius = 0.1;
        if isfield(options, 'radius'); radius = options.radius; end
        
        
        e                   = [1 2*radius 2*radius 2*radius 2*xshift 2*yshift 2*zshift 0 0 0];
        out                 = phantom3(e, max(res));
        out(out < 0)        = 0;
        out                 = flip(permute(flip(out,2), [2,1,3]),1);
        
        
        needScaling         = true;
        slice2D             = round(max(res)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_%g_%g_%g_v%s', shape, XMNP, options.interpMethod, xshift, yshift, zshift, radius, vers);
        
        
    case 'fwhmdots'
        vers = '0.2';
        
        % 2 spheres, in the middle
        
        % Modifiers:
        % gap       distance of the each spheres to center point
        % radius    radius of the spheres
        %
        % phantom is created on resolution
        % [max(resolution), max(resolution), max(resolution)] and
        % scaled down to requested resolution
        
        baseRes = max(res(:));
        
        gap = 0.1; % distance of spheres
        if isfield(options, 'gap'); gap = options.gap; end
        xshift = 0; % shift in x direction
        if isfield(options, 'xshift'); xshift = options.xshift; end
        yshift = 0; % shift in y direction
        if isfield(options, 'yshift'); yshift = options.yshift; end
        zshift = 0; % shift in z direction
        if isfield(options, 'zshift'); zshift = options.zshift; end
        radius = 0.025; % sphere radius
        if isfield(options, 'radius'); radius = options.radius; end
        
        
        e =    [
            1  2*radius  2*radius  2*radius   gap+2*xshift  2*yshift  2*zshift  0  0  0
            1  2*radius  2*radius  2*radius  -gap+2*xshift  2*yshift  2*zshift  0  0  0];
        out                 = phantom3(e, baseRes);
        out(out < 0)      	= 0;
        out                 = flip(permute(flip(out,2), [2,1,3]),1);
        
        
        needScaling         = true;
        slice2D             = round(max(res)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_%g_%g_%g_%g_v%s', shape, XMNP, options.interpMethod, gap, xshift, yshift, zshift, radius, vers);
        
        
    case 'sensitivitySpheres'
        vers = '0.2';
        
        % 4 spheres, in the middle, the edge, the side and the corner of
        % the threedimensional region of interest.
        
        % Modifiers:
        % gap       distance of the outer spheres to the roi edge
        % radius    radius of the spheres
        %
        % phantom is created on resolution
        % [max(resolution), max(resolution), max(resolution)] and
        % scaled down to requested resolution
        
        gap = 0.1;
        if isfield(options, 'gap'); gap = options.gap; end
        radius = 0.1;
        if isfield(options, 'radius'); radius = options.radius; end
        
        
        midXYZ              = [0 0 0];
        sideXYZ             = [1-radius-gap 0  0];
        edgeXYZ             = [1-radius-gap 1-radius-gap 0];
        cornerXYZ           = [1-radius-gap 1-radius-gap 1-radius-gap];
        e =    [
            1  2*radius  2*radius  2*radius    sideXYZ 0  0  0
            1  2*radius  2*radius  2*radius    edgeXYZ 0  0  0
            1  2*radius  2*radius  2*radius     midXYZ 0  0  0
            1  2*radius  2*radius  2*radius  cornerXYZ 0  0  0];
        out                 = phantom3(e, max(res));
        out(out < 0)      	= 0;
        out                 = flip(permute(flip(out,2), [2,1,3]),1);
        
        
        needScaling         = true;
        slice2D             = round(max(res)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%g_%g_v%s', shape, XMNP, options.interpMethod, gap, radius, vers);
        
        
    case 'random'
        vers = '0.2';
        
        % random pattern
        
        % Modifiers:
        % seed      initializes random number generator with given seed
        
        seed = round(rand*1e7);
        if isfield(options, 'seed'); seed = options.seed; end
        
        
        rng(seed);
        out                 = rand(res);
        
        
        needScaling         = false;
        slice2D             = round(res(3)/2);
        if ~isfield(options, 'interpMethod'); options.interpMethod = 'linear'; end
        if dim==2; out = squeeze(out(:,:,slice2D)); end
        uniquePhantomName   = sprintf('%s_%g_%s_%0.f_v%s', shape, XMNP, options.interpMethod, seed, vers);
        
        
    otherwise
        error('the provided phantom type is unknown');
        
        
end


%% check if selected phantom has all required definitions/flags set
if exist('uniquePhantomName','var') == 0
    error('The unique phantom name is not set, please check the code of the phantom creation.');
end
if exist('needScaling','var') == 0
    error('the needScaling flag is not set, please check the code of the phantom creation.');
end
if exist('slice2D','var') == 0
    error('the variable slice2D is not set, please check the code of the phantom creation.');
end


%% rescale resolution
if needScaling
    if dim==2
        [x,y]=meshgrid(linspace(1,size(out,2),res(2)),...
            linspace(1,size(out,1),res(1)));
        out=interp2(out,x,y,options.interpMethod);
    end
    if dim==3
        
        slice2D = floor(interp1([1,size(out,3)],[1,res(3)], slice2D));
        
        [y,x,z]=ndgrid(linspace(1,size(out,1),res(1)),...
            linspace(1,size(out,2),res(2)),...
            linspace(1,size(out,3),res(3)));
        out=interp3(out,x,y,z,options.interpMethod);
    end
end


%% rescale particle amount
XMNPTemp = sum(out(:));
if any(out(:))
    out = out * (XMNP/XMNPTemp);
end


%% show created phantom
if showPhantom
    
    cuts.x      = [1,res(1)];
    cuts.y      = [1,res(2)];
    cuts.z      = [1,slice2D];
    
    fid         = gcf; cla;
    visualizeMRX('volume', out, 'fid', fid, 'cuts', cuts);
    colorbar;
    fid.Name    = uniquePhantomName;
end


%% example
    function out = runExample()
        
        res                     = [50,50,1];
        shape                   = 'tumorGradient';
        options.XMNP            = 100;
        options.densityA        = .5;
        options.densityA1       = 0;
        options.densityA2       = .75;
        options.densityB        = .75;
        options.densityC        = 1;
        
        out = createPhantom(res, shape, 'options', options, 'showPhantom', true);
    end


end

